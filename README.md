# Ansible Ubuntu setup
Ansible scripts to setup dev environment on Ubuntu 17.10.

## Requirements
- Git
- Ansible 2+ (automatically installed from [Ansible offical PPA](https://launchpad.net/~ansible/+archive/ubuntu/ansible) with the provided install.sh script)

## Usage
```
sudo apt-get install git
git clone https://minche@bitbucket.org/minche/ubuntu-ansible-dev.git
cd ubuntu-ansible-dev
./install.sh
```

Modified from https://github.com/Benoth/ansible-ubuntu